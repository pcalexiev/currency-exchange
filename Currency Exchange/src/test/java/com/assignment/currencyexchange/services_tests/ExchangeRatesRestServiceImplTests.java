package com.assignment.currencyexchange.services_tests;

import com.assignment.currencyexchange.Factory;
import com.assignment.currencyexchange.exceptions.InvalidAmountException;
import com.assignment.currencyexchange.exceptions.InvalidCurrencyException;
import com.assignment.currencyexchange.exceptions.InvalidPageNumberException;
import com.assignment.currencyexchange.services.ExchangeRatesRestServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRatesRestServiceImplTests {

    @Mock
    RestTemplate mockRestTemplate;

    @InjectMocks
    ExchangeRatesRestServiceImpl restService;

    @Test
    public void getExchangeRate_ShouldReturnRate_WhenInputIsCorrect() {

        //Arrange
        String sourceCurrency = Factory.createTestTransaction().getSourceCurrency();
        String targetCurrency = Factory.createTestTransaction().getTargetCurrency();

        Mockito.when(mockRestTemplate.getForObject(anyString(), any())).thenReturn(Factory.createTestExchangeRate());

        //Act
        Double rate = restService.getExchangeRate(sourceCurrency, targetCurrency);

        //Assert
        Assert.assertEquals(rate, Double.valueOf(2.0));
    }

    @Test
    public void getExchangeRate_ShouldThrow_WhenBothCurrenciesAreTheSame() {

        //Arrange
        String sourceCurrency = Factory.createTestTransaction().getSourceCurrency();
        String targetCurrency = Factory.createTestTransaction().getSourceCurrency();

        //Act, Assert
        Assert.assertThrows(InvalidCurrencyException.class,
                () -> restService.getExchangeRate(sourceCurrency, targetCurrency));
    }

    @Test
    public void convertCurrency_ShouldReturnAmount_WhenInputIsCorrect() {

        //Arrange
        String sourceCurrency = Factory.createTestTransaction().getSourceCurrency();
        String targetCurrency = Factory.createTestTransaction().getTargetCurrency();

        Mockito.when(mockRestTemplate.getForObject(anyString(), any())).thenReturn(Factory.createTestExchangeRate());

        //Act
        Double[] bothAmounts = restService.convertCurrency(
                sourceCurrency,
                targetCurrency,
                Factory.createTestTransaction().getSourceAmount().toString()
        );
        Double targetAmount = bothAmounts[1];

        //Assert
        Assert.assertEquals(targetAmount, Double.valueOf(2.0));
    }

    @Test
    public void convertCurrency_ShouldThrow_WhenBothCurrenciesAreTheSame() {

        //Arrange
        String sourceCurrency = Factory.createTestTransaction().getSourceCurrency();
        String targetCurrency = Factory.createTestTransaction().getSourceCurrency();

        //Act, Assert
        Assert.assertThrows(InvalidCurrencyException.class,
                () -> restService.convertCurrency(
                        sourceCurrency,
                        targetCurrency,
                        Factory.createTestTransaction().getSourceAmount().toString()
                ));
    }

    @Test
    public void convertCurrency_Should_Throw_AmountIsNonNumber(){

        //Arrange
        String sourceCurrency = Factory.createTestTransaction().getSourceCurrency();
        String targetCurrency = Factory.createTestTransaction().getSourceCurrency();

        //Act, Assert
        Assert.assertThrows(InvalidAmountException.class,
                () -> restService.convertCurrency(
                        sourceCurrency,
                        targetCurrency,
                        "dasd"
                ));
    }

    @Test
    public void convertCurrency_Should_Throw_AmountIsSmallerThan_1(){

        //Arrange
        String sourceCurrency = Factory.createTestTransaction().getSourceCurrency();
        String targetCurrency = Factory.createTestTransaction().getSourceCurrency();

        //Act, Assert
        Assert.assertThrows(InvalidAmountException.class,
                () -> restService.convertCurrency(
                        sourceCurrency,
                        targetCurrency,
                        "0"
                ));
    }
}

package com.assignment.currencyexchange.services_tests;

import com.assignment.currencyexchange.Factory;
import com.assignment.currencyexchange.exceptions.EntityNotFoundException;
import com.assignment.currencyexchange.exceptions.InvalidPageNumberException;
import com.assignment.currencyexchange.exceptions.WrongDateFormatException;
import com.assignment.currencyexchange.models.Transaction;
import com.assignment.currencyexchange.repositories.TransactionRepository;
import com.assignment.currencyexchange.services.TransactionServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTests {

    @Mock
    TransactionRepository mockTransactionRepository;

    @InjectMocks
    TransactionServiceImpl transactionService;

    @Test
    public void createTransaction_Should_CallRepository(){

        //Arrange
        Transaction transaction = Factory.createTestTransaction();

        //Act
        transactionService.createTransaction(transaction);

        //Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1)).saveAndFlush(transaction);
    }

    @Test
    public void getTransactionById_Should_ReturnTransaction_WhenInputIsCorrect(){

        Transaction expectedTransaction = Factory.createTestTransaction();

        //Arrange
        Mockito.when(mockTransactionRepository.getById(anyString())).thenReturn(expectedTransaction);

        //Act
        Transaction  returnedTransaction = transactionService.getTransactionById("id");

        //Assert
        Assert.assertSame(expectedTransaction,returnedTransaction);
    }

    @Test
    public void getTransactionById_Should_Throw_WhenIdDoesNotExist(){

        //Arrange
        Mockito.when(mockTransactionRepository.getById(anyString())).thenReturn(null);

        //Act, Assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> transactionService.getTransactionById("id"));
    }

    @Test
    public void getTransactionsByDate_Should_CallRepository_WhenInputIsCorrect(){

        //Arrange
        Transaction transaction = Factory.createTestTransaction();
        Page<Transaction> page = new PageImpl<>(Collections.singletonList(transaction));
        Mockito.when(mockTransactionRepository.getAllByDate(any(),any())).thenReturn(page);

        //Act
        transactionService.getTransactionsByDate("02.10.2020","1");

        //Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1)).getAllByDate(any(),any());
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenDayIsSmallerThan_1(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("0.10.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenDayIsLargerThan_31(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("32.10.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenDayIsNonNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("da.10.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenDayIsNonWholeNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("5,2.10.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenMonthIsSmallerThan_1(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.0.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenMonthIsLargerThan_12(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.13.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenMonthIsNonNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.sa.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenMonthIsNonWholeNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.4,2.2020","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenYearIsSmallerThan_1(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.02.0","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenYearIsNonNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.02.da","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenYearIsNonWholeNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.02.52,3","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenDateHasMoreParts(){

        //Arrange, Act, Assert
        Assert.assertThrows(WrongDateFormatException.class,
                () -> transactionService.getTransactionsByDate("02.02.2.3","1"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenPageNumberIsSmallerThan_1(){

        //Arrange, Act, Assert
        Assert.assertThrows(InvalidPageNumberException.class,
                () -> transactionService.getTransactionsByDate("02.02.2020","0"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenPageNumberIsNonNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(InvalidPageNumberException.class,
                () -> transactionService.getTransactionsByDate("02.02.2020","da"));
    }

    @Test
    public void getTransactionsByDate_Should_Throw_WhenPageNumberIsNonWholeNumber(){

        //Arrange, Act, Assert
        Assert.assertThrows(InvalidPageNumberException.class,
                () -> transactionService.getTransactionsByDate("02.02.2020","54.5"));
    }
}

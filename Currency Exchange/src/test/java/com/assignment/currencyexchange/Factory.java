package com.assignment.currencyexchange;

import com.assignment.currencyexchange.models.Transaction;
import com.assignment.currencyexchange.models.network_models.ExchangeRate;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Factory {

    public static Transaction createTestTransaction(){
        Transaction transaction = new Transaction();

        transaction.setId(UUID.randomUUID().toString());
        transaction.setSourceAmount(1.0);
        transaction.setSourceCurrency("BGN");
        transaction.setTargetAmount(2.0);
        transaction.setTargetCurrency("EUR");
        transaction.setDate(LocalDate.now());

        return transaction;
    }

    public static ExchangeRate createTestExchangeRate(){
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setBase("BGN");
        exchangeRate.setDate("2020-10-08");
        Map<String,Double> map = new HashMap<>();
        map.put("EUR",2.0);
        exchangeRate.setRates(map);
        return exchangeRate;
    }
}

package com.assignment.currencyexchange.controllers.rest_controllers;

import com.assignment.currencyexchange.exceptions.EntityNotFoundException;
import com.assignment.currencyexchange.exceptions.NotRespondingException;
import com.assignment.currencyexchange.exceptions.RestApiInvalidFormatException;
import com.assignment.currencyexchange.models.Transaction;
import com.assignment.currencyexchange.models.data_transfer_objects.CurrencyDTO;
import com.assignment.currencyexchange.models.data_transfer_objects.RestApiErrorDTO;
import com.assignment.currencyexchange.services.contracts.CurrencyExchangeRestService;
import com.assignment.currencyexchange.services.contracts.TransactionService;
import com.assignment.currencyexchange.models.mappers.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/transactions")
public class TransactionsRestController {

    private final CurrencyExchangeRestService restService;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final Logger logger = Logger.getLogger(TransactionsRestController.class.getName());

    @Autowired
    public TransactionsRestController(CurrencyExchangeRestService restService,
                                      TransactionService transactionService,
                                      TransactionMapper transactionMapper) {
        this.restService = restService;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping("/rate")
    public Object findExchangeRate(@RequestParam String sourceCurrency,
                                   @RequestParam String targetCurrency) {
        try {
            Double rate = restService.getExchangeRate(sourceCurrency, targetCurrency);
            return new CurrencyDTO(rate);
        } catch (RestApiInvalidFormatException | NotRespondingException e) {
            logger.warning(e.getMessage());
            return new RestApiErrorDTO(e.getMessage());
        }
    }

    @PostMapping("/convert")
    public Object convertAmount(@RequestParam String sourceCurrency,
                                @RequestParam String targetCurrency,
                                @RequestParam String sourceAmount) {
        try {
            Double[] bothAmounts = restService.convertCurrency(sourceCurrency, targetCurrency, sourceAmount);
            Double sourceAmountInDouble = bothAmounts[0];
            Double targetAmount = bothAmounts[1];

            Transaction transaction =
                    transactionMapper
                            .dtoToTransaction(sourceCurrency,
                                    targetCurrency,
                                    sourceAmountInDouble,
                                    targetAmount);

            transaction = transactionService.createTransaction(transaction);

            return transactionMapper.transactionToDto(transaction);

        } catch (RestApiInvalidFormatException | NotRespondingException e) {
            logger.warning(e.getMessage());
            return new RestApiErrorDTO(e.getMessage());
        }
    }

    @GetMapping("/records/filter")
    public List<? extends Object> filterByDate(@RequestParam String date,
                                               @RequestParam String page) {

        try {
            return transactionService.getTransactionsByDate(date, page);
        } catch (RestApiInvalidFormatException e) {
            logger.warning(e.getMessage());
            return Collections.singletonList(new RestApiErrorDTO(e.getMessage()));
        }
    }

    @GetMapping("/records")
    public Object getById(@RequestParam String transactionId) {

        try {
            return transactionService.getTransactionById(transactionId);
        } catch (EntityNotFoundException e) {
            logger.warning(e.getMessage());
            return new RestApiErrorDTO(e.getMessage());
        }
    }
}

package com.assignment.currencyexchange.models.mappers;

import com.assignment.currencyexchange.models.Transaction;
import com.assignment.currencyexchange.models.data_transfer_objects.TransactionDTO;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class TransactionMapper {

    public Transaction dtoToTransaction(String sourceCurrency,
                                        String targetCurrency,
                                        Double sourceAmount,
                                        Double targetAmount){

        Transaction transaction = new Transaction();

        transaction.setDate(LocalDate.now());
        transaction.setSourceCurrency(sourceCurrency);
        transaction.setTargetCurrency(targetCurrency);
        transaction.setSourceAmount(sourceAmount);
        transaction.setTargetAmount(targetAmount);

        return transaction;
    }

    public TransactionDTO transactionToDto(Transaction transaction){
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setId(transaction.getId());
        transactionDTO.setTargetAmount(transaction.getTargetAmount());
        return  transactionDTO;
    }
}

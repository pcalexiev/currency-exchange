package com.assignment.currencyexchange.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "transactions")
@NoArgsConstructor
@Getter
@Setter
public class Transaction {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "transaction_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "source_amount")
    private Double sourceAmount;

    @Column(name = "source_currency")
    private String sourceCurrency;

    @Column(name = "target_amount")
    private Double targetAmount;

    @Column(name = "target_currency")
    private String targetCurrency;

    @Column(name = "transaction_date")
    private LocalDate date;
}

package com.assignment.currencyexchange.models.data_transfer_objects;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class RestApiErrorDTO {
    private String error;
}

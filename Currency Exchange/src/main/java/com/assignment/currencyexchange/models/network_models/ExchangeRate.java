package com.assignment.currencyexchange.models.network_models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class ExchangeRate {
    private Map<String, Double> rates;
    private String base;
    private String date;
}

package com.assignment.currencyexchange.models.data_transfer_objects;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class TransactionDTO {
    private String id;
    private Double targetAmount;
}

package com.assignment.currencyexchange.exceptions;

public class InvalidPageNumberException extends RestApiInvalidFormatException {

    public final static String INVALID_PAGE_NUMBER_MESSAGE = "Page number must be whole, positive, non zero number.";

    public InvalidPageNumberException(String message){
        super(message);
    }

}

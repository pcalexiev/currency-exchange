package com.assignment.currencyexchange.exceptions;

public class InvalidCurrencyException extends RestApiInvalidFormatException {

    public static final String INVALID_CURRENCY_MESSAGE = "One or both currencies are invalid. Please check the spelling.";
    public static final String IDENTICAL_CURRENCY_MESSAGE = "You must use different currencies";

    public InvalidCurrencyException(String message){
        super(message);
    }
}

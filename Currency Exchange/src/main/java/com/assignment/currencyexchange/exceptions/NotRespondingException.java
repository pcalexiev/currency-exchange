package com.assignment.currencyexchange.exceptions;

public class NotRespondingException extends RuntimeException {

    public static final String NOT_RESPONDING_MESSAGE = "Service is currently unavailable. Please try again later.";

    public NotRespondingException(String message){
        super(message);
    }
}

package com.assignment.currencyexchange.exceptions;

public class InvalidAmountException extends RestApiInvalidFormatException {

    public final static String INVALID_AMOUNT_MESSAGE = "Amount must be positive, non zero number";

    public InvalidAmountException(String message) {
        super(message);
    }
}

package com.assignment.currencyexchange.exceptions;

public class RestApiInvalidFormatException extends RuntimeException {

    public RestApiInvalidFormatException(String message){
        super(message);
    }

}

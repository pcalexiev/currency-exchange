package com.assignment.currencyexchange.exceptions;

public class WrongDateFormatException extends RestApiInvalidFormatException {

    public static final String ERROR_MESSAGE = "Wrong date format. Date format must be DD.MM.YYYY";

    public WrongDateFormatException(String message) {
        super(message);
    }
}

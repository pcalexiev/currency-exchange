package com.assignment.currencyexchange.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public final static String INCORRECT_ID_MESSAGE = "Transaction with %s:%s does not exist.";

    public EntityNotFoundException(String message){
        super(message);
    }

}

package com.assignment.currencyexchange.services.contracts;

import com.assignment.currencyexchange.models.Transaction;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface TransactionService {

    Transaction createTransaction(Transaction transaction);

    Transaction getTransactionById(String transactionId);

    List<Transaction> getTransactionsByDate(String date, String page);
}

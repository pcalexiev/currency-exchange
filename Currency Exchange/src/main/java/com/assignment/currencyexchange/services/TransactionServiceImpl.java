package com.assignment.currencyexchange.services;

import com.assignment.currencyexchange.exceptions.EntityNotFoundException;
import com.assignment.currencyexchange.exceptions.InvalidPageNumberException;
import com.assignment.currencyexchange.exceptions.WrongDateFormatException;
import com.assignment.currencyexchange.models.Transaction;
import com.assignment.currencyexchange.repositories.TransactionRepository;
import com.assignment.currencyexchange.services.contracts.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction createTransaction(Transaction transaction) {
        return transactionRepository.saveAndFlush(transaction);
    }

    @Override
    public Transaction getTransactionById(String transactionId) {

        Transaction transaction = transactionRepository.getById(transactionId);

        if (transaction == null) {
            throw new EntityNotFoundException(
                    String.format(EntityNotFoundException.INCORRECT_ID_MESSAGE, "ID", transactionId)
            );
        }

        return transaction;
    }

    @Override
    public List<Transaction> getTransactionsByDate(String date, String page) {

        String dateSeparator = "\\.";

        if (!checkDateIsInCorrectFormat(date, dateSeparator)) {
            throw new WrongDateFormatException(WrongDateFormatException.ERROR_MESSAGE);
        }

        if (!checkPageIsInCorrectFormat(page)) {
            throw new InvalidPageNumberException(InvalidPageNumberException.INVALID_PAGE_NUMBER_MESSAGE);
        }

        int pageNumber = Integer.parseInt(page);
        int[] dateParts = Arrays.stream(date.split(dateSeparator)).mapToInt(Integer::parseInt).toArray();

        LocalDate searchedDate = LocalDate.of(
                dateParts[2],
                dateParts[1],
                dateParts[0]);

        return transactionRepository
                .getAllByDate(searchedDate, PageRequest.of(pageNumber - 1, 8))
                .getContent();
    }

    private boolean checkDateIsInCorrectFormat(String date, String separator) {
        String[] dateParts = date.split(separator);

        if (dateParts.length != 3) {
            return false;
        }

        try {
            Integer.parseInt(dateParts[0]);
            Integer.parseInt(dateParts[1]);
            Integer.parseInt(dateParts[2]);
        } catch (NumberFormatException e) {
            return false;
        }

        int day = Integer.parseInt(dateParts[0]);
        int month = Integer.parseInt(dateParts[1]);
        int year = Integer.parseInt(dateParts[2]);

        if (day < 1 || day > 31 || month < 1 || month > 12 || year < 1) {
            return false;
        }

        return true;
    }

    private boolean checkPageIsInCorrectFormat(String page) {
        try {
            Integer.parseInt(page);
        } catch (NumberFormatException e) {
            return false;
        }

        int pageNumber = Integer.parseInt(page);

        if (pageNumber < 1) {
            return false;
        }

        return true;
    }
}

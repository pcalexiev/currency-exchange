package com.assignment.currencyexchange.services.contracts;


public interface CurrencyExchangeRestService {

    Double getExchangeRate(String sourceCurrency, String targetCurrency);

    Double[] convertCurrency(String sourceCurrency, String targetCurrency, String sourceAmount);

}

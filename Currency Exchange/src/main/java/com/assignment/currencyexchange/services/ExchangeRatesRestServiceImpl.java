package com.assignment.currencyexchange.services;


import com.assignment.currencyexchange.exceptions.InvalidAmountException;
import com.assignment.currencyexchange.exceptions.InvalidCurrencyException;
import com.assignment.currencyexchange.exceptions.NotRespondingException;
import com.assignment.currencyexchange.models.network_models.ExchangeRate;
import com.assignment.currencyexchange.services.contracts.CurrencyExchangeRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class ExchangeRatesRestServiceImpl implements CurrencyExchangeRestService {

    private final static String BASE_URL = "https://api.exchangeratesapi.io/";
    private final RestTemplate restTemplate;

    @Autowired
    public ExchangeRatesRestServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    //    https://api.exchangeratesapi.io/latest?base=EUR&symbols=USD
    @Override
    public Double getExchangeRate(String sourceCurrency, String targetCurrency) {

        if(sourceCurrency.equals(targetCurrency)){
            throw new InvalidCurrencyException(InvalidCurrencyException.IDENTICAL_CURRENCY_MESSAGE);
        }

        String exchangeRateURL = BASE_URL + "latest";

        String fullUrl = UriComponentsBuilder
                .fromUriString(exchangeRateURL)
                .queryParam("base", sourceCurrency)
                .queryParam("symbols", targetCurrency)
                .toUriString();

        try {
            ExchangeRate exchangeRate = restTemplate.getForObject(fullUrl, ExchangeRate.class);
            return exchangeRate.getRates().get(targetCurrency);
        }catch (ResourceAccessException ex){
            throw new NotRespondingException(NotRespondingException.NOT_RESPONDING_MESSAGE);
        } catch (HttpClientErrorException e){
            throw new InvalidCurrencyException(InvalidCurrencyException.INVALID_CURRENCY_MESSAGE);
        }
    }

    @Override
    public Double[] convertCurrency(String sourceCurrency, String targetCurrency, String sourceAmount) {

        if(!checkAmountIsInCorrectFormat(sourceAmount)){
            throw new InvalidAmountException(InvalidAmountException.INVALID_AMOUNT_MESSAGE);
        }

        double sourceAmountInDouble = Double.parseDouble(sourceAmount);

        Double exchangeRate = getExchangeRate(sourceCurrency,targetCurrency);

        Double[] bothAmounts = new Double[2];
        bothAmounts[0] = sourceAmountInDouble;
        bothAmounts[1] = sourceAmountInDouble * exchangeRate;

        return bothAmounts;
    }

    private boolean checkAmountIsInCorrectFormat(String sourceAmount){
        try{
            Double.parseDouble(sourceAmount);
        }catch (NumberFormatException e){
            return false;
        }

        double amount = Double.parseDouble(sourceAmount);

        if(amount<1){
            return false;
        }

        return true;
    }

}

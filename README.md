# Currency Exchange

## Description
Currency Exchange is a REST API application, which allows users to check exchange rates of currencies and calculate amounts for the specific rates. The returned information is in JSON format.
The application connects to the free for use REST API of https://exchangeratesapi.io/

When creating a conversion between two currencies, the data is stored in the application and can be later accessed. You can access all records from a specific date or find a specific conversion by it's ID.

## The application supports the following operations:
- Find exchange rate for two currencies
- Convert given amount from one currency to another
- Get all past conversions for a specific date
- View a past conversion based on the ID

## Find exchange rate for two currencies

- EXAMPLE: http://localhost:8080/api/transactions/rate?sourceCurrency=BGN&targetCurrency=USD

This is a GET request and takes two request parameters:
- sourceCurrency
- targetCurrency

In order for the request to be successfull:
- the currencies have to be existing currencies 
- the currencies have to be different from each other

In case of success, the application returns a JSON object with the following attributes:
- rate : Double 

In case of failure the application returns a JSON object with the following attributes:
- error : String

## Convert given amount from one currency to another

- EXAMPLE: http://localhost:8080/api/transactions/convert?sourceCurrency=EUR&targetCurrency=BGN&sourceAmount=100

This is a POST request and takes three request parameters:
- sourceCurrency
- targetCurrency
- sourceAmount

In order for the request to be successfull:
- the currencies have to be existing currencies 
- the currencies have to be different from each other
- the amount must be a positive non zero number

In case of success, the application returns a JSON object with the following attributes:
- id : String
- targetAmount : Double

In case of failure the application returns a JSON object with the following attributes:
- error : String

## Get all past conversions for a specific date

- EXAMPLE: http://localhost:8080/api/transactions/records/filter?date=02.09.2020&page=1

This is a GET request and takes two request parameters:
- date
- page

The results will be provided in pages consisting of 8 records and the parameter page will determine, which page from the results to return.

In order for the request to be successfull:
- the date must be in the format of DD.MM.YYYY or D.M.YYYY 
- the page must be a positive non zero whole number

In case of success, the application returns a list of JSON objects with the following attributes:
- id: String
- sourceAmount: Double
- sourceCurrency: String
- targetAmount: Double
- targetCurrency: String
- date: String

In case of failure the application returns a JSON object with the following attributes:
- error : String

## View a past conversion based on the ID

- EXAMPLE: http://localhost:8080/api/transactions/records?transactionId=e3729ffa-ed06-4660-945a-a4b7200c331b

This is a GET request and takes one request parameter:
- transactionId

In order for the request to be successfull:
- The ID has to be an existing ID in the database.

In case of success, the application returns a JSON object with the following attributes:
- id: String
- sourceAmount: Double
- sourceCurrency: String
- targetAmount: Double
- targetCurrency: String
- date: String

In case of failure the application returns a JSON object with the following attributes:
- error : String

# Have fun using the application

Trello: https://trello.com/b/pkjiMB6x/currency-exchange

